// console.log("sup");

let number = Number(prompt("INPUT NUMBER:"));
console.log("The number you provided is " + number + ".");

for (let count = number; count >= 0; count--) {

	if (count <= 50) {
		console.log("Value is lower/equal to 50. Terminating loop.");
		break;

	} else if (count % 10 === 0) {

		console.log("Number is divisible by 10. Skipping number.");
		continue;

	} else if (count % 5 === 0) { 
        console.log(count);

    }
}

let letters = 'supercalifragilisticexpialidocious';
console.log(letters);

let result = '';

for (let x = 0; x < letters.length; x++) {

	if (letters[x].toLowerCase() == 'a' ||
		letters[x].toLowerCase() == 'e' ||
		letters[x].toLowerCase() == 'i' ||
		letters[x].toLowerCase() == 'o' ||
		letters[x].toLowerCase() == 'u') {

		continue;

	} else {
		result += letters[x];

	}
}

console.log(result);